# Binary-Net-Scores
'''
Binary classifier that takes a vector of model scores as input, 
and returns whether or not the vector corresponds to a best match image in results.
'''
