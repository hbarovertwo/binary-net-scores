from __future__ import division
from keras.models import Sequential
from keras.layers import Dense, Dropout
import pandas as pd
import numpy as np

#load raw CSV into pandas dataframe
ds1 = pd.read_csv('orders_5.csv', sep='\t', lineterminator='\n', header=None)

#manually balance data to include 1's and 0's for true labels in near equal amounts
one1 = ds1[ds1[28]==1]
zero1 = ds1[ds1[28]==0][:len(one1)]

#combine the two sub-dataframes into a single dataframe
df1 = pd.concat([one1,zero1],ignore_index=True)

#shuffle the dataframe
df1 = df1.sample(frac=1).reset_index(drop=True)

ds2 = pd.read_csv('orders_6.csv', sep='\t', lineterminator='\n', header=None)

one2 = ds2[ds2[28]==1]
zero2 = ds2[ds2[28]==0][:len(one2)]

df2 = pd.concat([one2,zero2], ignore_index=True)

df2 = df2.sample(frac=1).reset_index(drop=True)

df = pd.concat([df1,df2], ignore_index=True)

#Use 28 features (excluding the 29th feature in this case) and the final column are true labels.
X = df.iloc[:, 0:28].values
Y = df.iloc[:, 28].values

from sklearn.model_selection import train_test_split

X_Train, X_Test, Y_Train, Y_Test = train_test_split(X, Y,
                                                   test_size=0.2,
                                                   random_state=42)

from sklearn.preprocessing import StandardScaler

sc_X = StandardScaler().fit(X_Train)
X_T = sc_X.transform(X_Train)
X_E = sc_X.transform(X_Test)

print((X_T.shape,Y_Train.shape))
print((X_E.shape, Y_Test.shape))

model = Sequential()
model.add(Dense(24, input_dim=28, activation='relu'))
model.add(Dense(12, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy',
             optimizer='adam',
             metrics=['accuracy'])

model.fit(X_T,Y_Train,epochs=100,batch_size=64)

score = model.evaluate(X_E, Y_Test, batch_size=64)

from sklearn.metrics import confusion_matrix

yhat = model.predict_classes(X_E)
matrix = confusion_matrix(Y_Test,yhat)
print(matrix)
tn, fp, fn, tp = matrix.ravel()
error_rate = (fp+fn)/(tn+tp+fn+fp)
recall = tp/(tp+fn)
specifity = tn/(tn+fp)
precision = tp/(tp+fp)
print('error rate: ' + str(error_rate),
      'recall: ' + str(recall),
      'specifity: ' + str(specifity),
      'precision: ' + str(precision))

model.save('binary_net_best.h5')
